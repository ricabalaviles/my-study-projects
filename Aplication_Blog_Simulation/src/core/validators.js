export class Validators {
    static required(value = ''){
        return value && value.trim()
    }

    static minLenfth(length){
        return value => {
            return value.length >= length
        }
    }

}